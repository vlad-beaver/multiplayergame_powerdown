// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef POWERDOWN_PowerDownCharacter_generated_h
#error "PowerDownCharacter.generated.h already included, missing '#pragma once' in PowerDownCharacter.h"
#endif
#define POWERDOWN_PowerDownCharacter_generated_h

#define PowerDown_Source_PowerDown_PowerDownCharacter_h_12_SPARSE_DATA
#define PowerDown_Source_PowerDown_PowerDownCharacter_h_12_RPC_WRAPPERS \
	virtual bool ServerCollectPickups_Validate(); \
	virtual void ServerCollectPickups_Implementation(); \
	virtual void OnPlayerDeath_Implementation(); \
 \
	DECLARE_FUNCTION(execOnRep_CurrentPower); \
	DECLARE_FUNCTION(execServerCollectPickups); \
	DECLARE_FUNCTION(execCollectPickups); \
	DECLARE_FUNCTION(execOnPlayerDeath); \
	DECLARE_FUNCTION(execUpdatePower); \
	DECLARE_FUNCTION(execGetCurrentPower); \
	DECLARE_FUNCTION(execGetInitialPower);


#define PowerDown_Source_PowerDown_PowerDownCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual bool ServerCollectPickups_Validate(); \
	virtual void ServerCollectPickups_Implementation(); \
	virtual void OnPlayerDeath_Implementation(); \
 \
	DECLARE_FUNCTION(execOnRep_CurrentPower); \
	DECLARE_FUNCTION(execServerCollectPickups); \
	DECLARE_FUNCTION(execCollectPickups); \
	DECLARE_FUNCTION(execOnPlayerDeath); \
	DECLARE_FUNCTION(execUpdatePower); \
	DECLARE_FUNCTION(execGetCurrentPower); \
	DECLARE_FUNCTION(execGetInitialPower);


#define PowerDown_Source_PowerDown_PowerDownCharacter_h_12_EVENT_PARMS
#define PowerDown_Source_PowerDown_PowerDownCharacter_h_12_CALLBACK_WRAPPERS
#define PowerDown_Source_PowerDown_PowerDownCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPowerDownCharacter(); \
	friend struct Z_Construct_UClass_APowerDownCharacter_Statics; \
public: \
	DECLARE_CLASS(APowerDownCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PowerDown"), NO_API) \
	DECLARE_SERIALIZER(APowerDownCharacter) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		InitialPower=NETFIELD_REP_START, \
		CollectionSphereRadius, \
		CurrentPower, \
		NETFIELD_REP_END=CurrentPower	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define PowerDown_Source_PowerDown_PowerDownCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPowerDownCharacter(); \
	friend struct Z_Construct_UClass_APowerDownCharacter_Statics; \
public: \
	DECLARE_CLASS(APowerDownCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PowerDown"), NO_API) \
	DECLARE_SERIALIZER(APowerDownCharacter) \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		InitialPower=NETFIELD_REP_START, \
		CollectionSphereRadius, \
		CurrentPower, \
		NETFIELD_REP_END=CurrentPower	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define PowerDown_Source_PowerDown_PowerDownCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APowerDownCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APowerDownCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APowerDownCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APowerDownCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APowerDownCharacter(APowerDownCharacter&&); \
	NO_API APowerDownCharacter(const APowerDownCharacter&); \
public:


#define PowerDown_Source_PowerDown_PowerDownCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APowerDownCharacter(APowerDownCharacter&&); \
	NO_API APowerDownCharacter(const APowerDownCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APowerDownCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APowerDownCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APowerDownCharacter)


#define PowerDown_Source_PowerDown_PowerDownCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(APowerDownCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(APowerDownCharacter, FollowCamera); } \
	FORCEINLINE static uint32 __PPO__CollectionSphere() { return STRUCT_OFFSET(APowerDownCharacter, CollectionSphere); } \
	FORCEINLINE static uint32 __PPO__InitialPower() { return STRUCT_OFFSET(APowerDownCharacter, InitialPower); } \
	FORCEINLINE static uint32 __PPO__BaseSpeed() { return STRUCT_OFFSET(APowerDownCharacter, BaseSpeed); } \
	FORCEINLINE static uint32 __PPO__SpeedFactor() { return STRUCT_OFFSET(APowerDownCharacter, SpeedFactor); } \
	FORCEINLINE static uint32 __PPO__CollectionSphereRadius() { return STRUCT_OFFSET(APowerDownCharacter, CollectionSphereRadius); } \
	FORCEINLINE static uint32 __PPO__CurrentPower() { return STRUCT_OFFSET(APowerDownCharacter, CurrentPower); }


#define PowerDown_Source_PowerDown_PowerDownCharacter_h_9_PROLOG \
	PowerDown_Source_PowerDown_PowerDownCharacter_h_12_EVENT_PARMS


#define PowerDown_Source_PowerDown_PowerDownCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PowerDown_Source_PowerDown_PowerDownCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	PowerDown_Source_PowerDown_PowerDownCharacter_h_12_SPARSE_DATA \
	PowerDown_Source_PowerDown_PowerDownCharacter_h_12_RPC_WRAPPERS \
	PowerDown_Source_PowerDown_PowerDownCharacter_h_12_CALLBACK_WRAPPERS \
	PowerDown_Source_PowerDown_PowerDownCharacter_h_12_INCLASS \
	PowerDown_Source_PowerDown_PowerDownCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PowerDown_Source_PowerDown_PowerDownCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PowerDown_Source_PowerDown_PowerDownCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	PowerDown_Source_PowerDown_PowerDownCharacter_h_12_SPARSE_DATA \
	PowerDown_Source_PowerDown_PowerDownCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	PowerDown_Source_PowerDown_PowerDownCharacter_h_12_CALLBACK_WRAPPERS \
	PowerDown_Source_PowerDown_PowerDownCharacter_h_12_INCLASS_NO_PURE_DECLS \
	PowerDown_Source_PowerDown_PowerDownCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POWERDOWN_API UClass* StaticClass<class APowerDownCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PowerDown_Source_PowerDown_PowerDownCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
