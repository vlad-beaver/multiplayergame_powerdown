// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef POWERDOWN_PowerDownGameMode_generated_h
#error "PowerDownGameMode.generated.h already included, missing '#pragma once' in PowerDownGameMode.h"
#endif
#define POWERDOWN_PowerDownGameMode_generated_h

#define PowerDown_Source_PowerDown_PowerDownGameMode_h_13_SPARSE_DATA
#define PowerDown_Source_PowerDown_PowerDownGameMode_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetPowerToWinMultiplier); \
	DECLARE_FUNCTION(execGetDecayRate);


#define PowerDown_Source_PowerDown_PowerDownGameMode_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetPowerToWinMultiplier); \
	DECLARE_FUNCTION(execGetDecayRate);


#define PowerDown_Source_PowerDown_PowerDownGameMode_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPowerDownGameMode(); \
	friend struct Z_Construct_UClass_APowerDownGameMode_Statics; \
public: \
	DECLARE_CLASS(APowerDownGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/PowerDown"), POWERDOWN_API) \
	DECLARE_SERIALIZER(APowerDownGameMode)


#define PowerDown_Source_PowerDown_PowerDownGameMode_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAPowerDownGameMode(); \
	friend struct Z_Construct_UClass_APowerDownGameMode_Statics; \
public: \
	DECLARE_CLASS(APowerDownGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/PowerDown"), POWERDOWN_API) \
	DECLARE_SERIALIZER(APowerDownGameMode)


#define PowerDown_Source_PowerDown_PowerDownGameMode_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	POWERDOWN_API APowerDownGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APowerDownGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(POWERDOWN_API, APowerDownGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APowerDownGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	POWERDOWN_API APowerDownGameMode(APowerDownGameMode&&); \
	POWERDOWN_API APowerDownGameMode(const APowerDownGameMode&); \
public:


#define PowerDown_Source_PowerDown_PowerDownGameMode_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	POWERDOWN_API APowerDownGameMode(APowerDownGameMode&&); \
	POWERDOWN_API APowerDownGameMode(const APowerDownGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(POWERDOWN_API, APowerDownGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APowerDownGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APowerDownGameMode)


#define PowerDown_Source_PowerDown_PowerDownGameMode_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PowerDrainDelay() { return STRUCT_OFFSET(APowerDownGameMode, PowerDrainDelay); } \
	FORCEINLINE static uint32 __PPO__DecayRate() { return STRUCT_OFFSET(APowerDownGameMode, DecayRate); } \
	FORCEINLINE static uint32 __PPO__PowerToWinMultiplier() { return STRUCT_OFFSET(APowerDownGameMode, PowerToWinMultiplier); }


#define PowerDown_Source_PowerDown_PowerDownGameMode_h_10_PROLOG
#define PowerDown_Source_PowerDown_PowerDownGameMode_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PowerDown_Source_PowerDown_PowerDownGameMode_h_13_PRIVATE_PROPERTY_OFFSET \
	PowerDown_Source_PowerDown_PowerDownGameMode_h_13_SPARSE_DATA \
	PowerDown_Source_PowerDown_PowerDownGameMode_h_13_RPC_WRAPPERS \
	PowerDown_Source_PowerDown_PowerDownGameMode_h_13_INCLASS \
	PowerDown_Source_PowerDown_PowerDownGameMode_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PowerDown_Source_PowerDown_PowerDownGameMode_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PowerDown_Source_PowerDown_PowerDownGameMode_h_13_PRIVATE_PROPERTY_OFFSET \
	PowerDown_Source_PowerDown_PowerDownGameMode_h_13_SPARSE_DATA \
	PowerDown_Source_PowerDown_PowerDownGameMode_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	PowerDown_Source_PowerDown_PowerDownGameMode_h_13_INCLASS_NO_PURE_DECLS \
	PowerDown_Source_PowerDown_PowerDownGameMode_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POWERDOWN_API UClass* StaticClass<class APowerDownGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PowerDown_Source_PowerDown_PowerDownGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
