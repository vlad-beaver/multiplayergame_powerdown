// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PowerDown/PowerDownGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePowerDownGameMode() {}
// Cross Module References
	POWERDOWN_API UClass* Z_Construct_UClass_APowerDownGameMode_NoRegister();
	POWERDOWN_API UClass* Z_Construct_UClass_APowerDownGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_PowerDown();
// End Cross Module References
	DEFINE_FUNCTION(APowerDownGameMode::execGetPowerToWinMultiplier)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetPowerToWinMultiplier();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APowerDownGameMode::execGetDecayRate)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetDecayRate();
		P_NATIVE_END;
	}
	void APowerDownGameMode::StaticRegisterNativesAPowerDownGameMode()
	{
		UClass* Class = APowerDownGameMode::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetDecayRate", &APowerDownGameMode::execGetDecayRate },
			{ "GetPowerToWinMultiplier", &APowerDownGameMode::execGetPowerToWinMultiplier },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APowerDownGameMode_GetDecayRate_Statics
	{
		struct PowerDownGameMode_eventGetDecayRate_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_APowerDownGameMode_GetDecayRate_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PowerDownGameMode_eventGetDecayRate_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APowerDownGameMode_GetDecayRate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APowerDownGameMode_GetDecayRate_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APowerDownGameMode_GetDecayRate_Statics::Function_MetaDataParams[] = {
		{ "Category", "Power" },
		{ "Comment", "// Get the current Rate of decay (% of InitialPower per sec)\n" },
		{ "ModuleRelativePath", "PowerDownGameMode.h" },
		{ "ToolTip", "Get the current Rate of decay (% of InitialPower per sec)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APowerDownGameMode_GetDecayRate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APowerDownGameMode, nullptr, "GetDecayRate", nullptr, nullptr, sizeof(PowerDownGameMode_eventGetDecayRate_Parms), Z_Construct_UFunction_APowerDownGameMode_GetDecayRate_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_APowerDownGameMode_GetDecayRate_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APowerDownGameMode_GetDecayRate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APowerDownGameMode_GetDecayRate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APowerDownGameMode_GetDecayRate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APowerDownGameMode_GetDecayRate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APowerDownGameMode_GetPowerToWinMultiplier_Statics
	{
		struct PowerDownGameMode_eventGetPowerToWinMultiplier_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_APowerDownGameMode_GetPowerToWinMultiplier_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PowerDownGameMode_eventGetPowerToWinMultiplier_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APowerDownGameMode_GetPowerToWinMultiplier_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APowerDownGameMode_GetPowerToWinMultiplier_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APowerDownGameMode_GetPowerToWinMultiplier_Statics::Function_MetaDataParams[] = {
		{ "Category", "Power" },
		{ "Comment", "// Access the power level required to win the game\n" },
		{ "ModuleRelativePath", "PowerDownGameMode.h" },
		{ "ToolTip", "Access the power level required to win the game" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APowerDownGameMode_GetPowerToWinMultiplier_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APowerDownGameMode, nullptr, "GetPowerToWinMultiplier", nullptr, nullptr, sizeof(PowerDownGameMode_eventGetPowerToWinMultiplier_Parms), Z_Construct_UFunction_APowerDownGameMode_GetPowerToWinMultiplier_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_APowerDownGameMode_GetPowerToWinMultiplier_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APowerDownGameMode_GetPowerToWinMultiplier_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APowerDownGameMode_GetPowerToWinMultiplier_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APowerDownGameMode_GetPowerToWinMultiplier()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APowerDownGameMode_GetPowerToWinMultiplier_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APowerDownGameMode_NoRegister()
	{
		return APowerDownGameMode::StaticClass();
	}
	struct Z_Construct_UClass_APowerDownGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PowerDrainDelay_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PowerDrainDelay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DecayRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DecayRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PowerToWinMultiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PowerToWinMultiplier;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APowerDownGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_PowerDown,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APowerDownGameMode_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APowerDownGameMode_GetDecayRate, "GetDecayRate" }, // 557448542
		{ &Z_Construct_UFunction_APowerDownGameMode_GetPowerToWinMultiplier, "GetPowerToWinMultiplier" }, // 1466404840
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APowerDownGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "PowerDownGameMode.h" },
		{ "ModuleRelativePath", "PowerDownGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APowerDownGameMode_Statics::NewProp_PowerDrainDelay_MetaData[] = {
		{ "Category", "Power" },
		{ "Comment", "// How many times per second to update character's power and check game rules\n" },
		{ "ModuleRelativePath", "PowerDownGameMode.h" },
		{ "ToolTip", "How many times per second to update character's power and check game rules" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APowerDownGameMode_Statics::NewProp_PowerDrainDelay = { "PowerDrainDelay", nullptr, (EPropertyFlags)0x0020080000010015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APowerDownGameMode, PowerDrainDelay), METADATA_PARAMS(Z_Construct_UClass_APowerDownGameMode_Statics::NewProp_PowerDrainDelay_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APowerDownGameMode_Statics::NewProp_PowerDrainDelay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APowerDownGameMode_Statics::NewProp_DecayRate_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "Power" },
		{ "Comment", "// The rate at which character's lose power (% of InitialPower per second)\n" },
		{ "ModuleRelativePath", "PowerDownGameMode.h" },
		{ "ToolTip", "The rate at which character's lose power (% of InitialPower per second)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APowerDownGameMode_Statics::NewProp_DecayRate = { "DecayRate", nullptr, (EPropertyFlags)0x0020080000010005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APowerDownGameMode, DecayRate), METADATA_PARAMS(Z_Construct_UClass_APowerDownGameMode_Statics::NewProp_DecayRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APowerDownGameMode_Statics::NewProp_DecayRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APowerDownGameMode_Statics::NewProp_PowerToWinMultiplier_MetaData[] = {
		{ "BlueprintProtected", "true" },
		{ "Category", "Power" },
		{ "Comment", "// The power level needed to win the game\n" },
		{ "ModuleRelativePath", "PowerDownGameMode.h" },
		{ "ToolTip", "The power level needed to win the game" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APowerDownGameMode_Statics::NewProp_PowerToWinMultiplier = { "PowerToWinMultiplier", nullptr, (EPropertyFlags)0x0020080000010005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APowerDownGameMode, PowerToWinMultiplier), METADATA_PARAMS(Z_Construct_UClass_APowerDownGameMode_Statics::NewProp_PowerToWinMultiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APowerDownGameMode_Statics::NewProp_PowerToWinMultiplier_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APowerDownGameMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APowerDownGameMode_Statics::NewProp_PowerDrainDelay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APowerDownGameMode_Statics::NewProp_DecayRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APowerDownGameMode_Statics::NewProp_PowerToWinMultiplier,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APowerDownGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APowerDownGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APowerDownGameMode_Statics::ClassParams = {
		&APowerDownGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_APowerDownGameMode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_APowerDownGameMode_Statics::PropPointers),
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_APowerDownGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APowerDownGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APowerDownGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APowerDownGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APowerDownGameMode, 765293924);
	template<> POWERDOWN_API UClass* StaticClass<APowerDownGameMode>()
	{
		return APowerDownGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APowerDownGameMode(Z_Construct_UClass_APowerDownGameMode, &APowerDownGameMode::StaticClass, TEXT("/Script/PowerDown"), TEXT("APowerDownGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APowerDownGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
