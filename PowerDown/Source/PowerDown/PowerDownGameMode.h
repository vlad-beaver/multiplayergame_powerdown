// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PowerDownGameState.h"
#include "GameFramework/GameModeBase.h"
#include "PowerDownGameMode.generated.h"

UCLASS(minimalapi)
class APowerDownGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APowerDownGameMode();

	// Start the power drain timer
	virtual void BeginPlay() override;

	// Get the current Rate of decay (% of InitialPower per sec)
	UFUNCTION(BlueprintPure, Category = "Power")
	float GetDecayRate();

	// Access the power level required to win the game
	UFUNCTION(BlueprintPure, Category = "Power")
	float GetPowerToWinMultiplier();

protected:
	// How many times per second to update character's power and check game rules
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Power")
	float PowerDrainDelay;
	
	// Access the timer for recurring draining
	FTimerHandle PowerDrainTimer;
	
	// The rate at which character's lose power (% of InitialPower per second)
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Power", Meta = (BlueprintProtected = "true"))
	float DecayRate;

	// The power level needed to win the game
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Power", Meta = (BlueprintProtected = "true"))
	float PowerToWinMultiplier;

	// Track the number of players who have run out of energy and been eliminated from the game
	int32 DeadPlayerCount;

private:
	// Drains power from character's and update gameplay
	void DrainPowerOverTime();

	// Track all the spawn volumes in the level
	TArray<class ASpawnVolume*> SpawnVolumesActors;

	// Handle any function calls for when the game transitions between states
	void HandleNewState(EBatteryPlayState NewState);
};



