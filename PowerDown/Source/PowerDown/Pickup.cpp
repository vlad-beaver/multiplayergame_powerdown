// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickup.h"
#include "Net/UnrealNetwork.h"

APickup::APickup()
{
	// Tell UE4 to replicate this Actor
	bReplicates = true;

	// Pickups do not need to tick every frame
	PrimaryActorTick.bCanEverTick = false;

	// StaticMeshActor disables overlap events by default, which we need to re-enable
	GetStaticMeshComponent()->SetGenerateOverlapEvents(true);

	if (GetLocalRole() == ROLE_Authority)
	{
		bIsActive = true;
	}
}

void APickup::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APickup, bIsActive);
}


bool APickup::IsActive()
{
	return bIsActive;
}

void APickup::SetActive(bool NewPickupState)
{
	if (GetLocalRole() == ROLE_Authority)
	{
		bIsActive = NewPickupState;
	}	
}

void APickup::OnRep_IsActive()
{
	
}

void APickup::WasCollected_Implementation()
{
	// Log a debug message
	UE_LOG(LogClass, Log, TEXT("APickup::WasCollected_Implementation %s"), *GetName());
}

void APickup::PickedUpBy(APawn* Pawn)
{
	if (GetLocalRole() == ROLE_Authority)
	{
		// Store the pawn who picked up the pickup
		PickupInstigator = Pawn;
		// Notify clients of the picked up action
		ClientOnPickedUpBy(Pawn);
	}
}

void APickup::ClientOnPickedUpBy_Implementation(APawn* Pawn)
{
	// Store the pawn who picked up the pickup (client)
	PickupInstigator = Pawn;
	// Fire the Blueprint Native Event, which itself cannot be replicated
	WasCollected();
}