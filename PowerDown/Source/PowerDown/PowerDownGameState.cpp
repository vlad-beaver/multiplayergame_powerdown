// Fill out your copyright notice in the Description page of Project Settings.


#include "PowerDownGameState.h"
#include "Net/UnrealNetwork.h"

APowerDownGameState::APowerDownGameState()
{
	// Set the default state when state is not currently known
	CurrentState = EBatteryPlayState::EUnknown;
}

void APowerDownGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APowerDownGameState, PowerToWin);
	DOREPLIFETIME(APowerDownGameState, CurrentState);
	DOREPLIFETIME(APowerDownGameState, WinningPlayerName);
}

EBatteryPlayState APowerDownGameState::GetCurrentState() const
{
	return CurrentState;
}

void APowerDownGameState::SetCurrentState(EBatteryPlayState NewState)
{
	if (GetLocalRole() == ROLE_Authority)
	{
		CurrentState = NewState;
	}
}

void APowerDownGameState::OnRep_CurrentState()
{
	
}



