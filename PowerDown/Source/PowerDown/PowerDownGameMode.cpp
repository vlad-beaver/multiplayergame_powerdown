// Copyright Epic Games, Inc. All Rights Reserved.

#include "PowerDownGameMode.h"
#include "PowerDownCharacter.h"
#include "PowerDownGameState.h"
#include "SpawnVolume.h"
#include "Kismet/GameplayStatics.h"

APowerDownGameMode::APowerDownGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// Set the type of GameState used in the game
	GameStateClass = APowerDownGameState::StaticClass();

	// Set the type of HUD used in the game
	// static ConstructorHelpers::FClassFinder<AHUD> PlayerHUDClass(TEXT("/Game/Blueprints/BP_PowerDownHUD"));
	// if (PlayerHUDClass.Class != NULL)
	// {
	// 	HUDClass = PlayerHUDClass.Class;
	// }

	// Base decay rate
	DecayRate = 0.02f;

	// Base value for how frequently to drain power
	PowerDrainDelay = 0.25f;

	// Set base value for the power multiplier
	PowerToWinMultiplier = 1.25f;

	// Reset stats
	DeadPlayerCount = 0;
}

void APowerDownGameMode::BeginPlay()
{
	Super::BeginPlay();
	
	GetWorldTimerManager().SetTimer(PowerDrainTimer, this, &APowerDownGameMode::DrainPowerOverTime, PowerDrainDelay, true);

	// Access the world to get to the players
	UWorld* World = GetWorld();
	check(World);
	APowerDownGameState* MyGameState = Cast<APowerDownGameState>(GameState);
	check(MyGameState);

	// Reset stats
	DeadPlayerCount = 0;

	// Gather up all the spawn volumes and store them from later
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(World, ASpawnVolume::StaticClass(), FoundActors);
	for (auto Actor : FoundActors)
	{
		if (ASpawnVolume* TestSpawnVol = Cast<ASpawnVolume>(Actor))
		{
			// Add the volume to the array and ensure it only exists in the array once
			SpawnVolumesActors.AddUnique(TestSpawnVol);
		}
	}

	// Transitioning the game to the playing state
	HandleNewState(EBatteryPlayState::EPlaying);
	
	//Go through all the characters in the game
	for (FConstControllerIterator It = World->GetControllerIterator(); It; ++It)
	{
		if (APlayerController* PlayerController = Cast<APlayerController>(*It))
		{
			if (APowerDownCharacter* BatteryCharacter = Cast<APowerDownCharacter>(PlayerController->GetPawn()))
			{
				MyGameState->PowerToWin = BatteryCharacter->GetInitialPower() * PowerToWinMultiplier;
				break;
			}
		}
	}
}

float APowerDownGameMode::GetDecayRate()
{
	return DecayRate;
}

float APowerDownGameMode::GetPowerToWinMultiplier()
{
	return PowerToWinMultiplier;
}

void APowerDownGameMode::DrainPowerOverTime()
{
	// Access the world to get to the players
	UWorld* World = GetWorld();
	check(World);
	APowerDownGameState* MyGameState = Cast<APowerDownGameState>(GameState);
	check(MyGameState);

	//Go through all the characters in the game
	for (FConstControllerIterator It = World->GetControllerIterator(); It; ++It)
	{
		if (APlayerController* PlayerController = Cast<APlayerController>(*It))
		{
			if (APowerDownCharacter* BatteryCharacter = Cast<APowerDownCharacter>(PlayerController->GetPawn()))
			{
				if (BatteryCharacter->GetCurrentPower() > MyGameState->PowerToWin)
				{
					MyGameState->WinningPlayerName = BatteryCharacter->GetName();
					HandleNewState(EBatteryPlayState::EWon);
				}
				else if (BatteryCharacter->GetCurrentPower() > 0)
				{
					BatteryCharacter->UpdatePower(-PowerDrainDelay*DecayRate*(BatteryCharacter->GetInitialPower()));
				}
				else
				{
					// Player died
					BatteryCharacter->OnPlayerDeath();

					// See if this is the last player to die
					++DeadPlayerCount;
					if (DeadPlayerCount >= GetNumPlayers())
					{
						HandleNewState(EBatteryPlayState::EGameOver);
					}
				}
			}
		}
	}
}

void APowerDownGameMode::HandleNewState(EBatteryPlayState NewState)
{
	APowerDownGameState* MyGameState = Cast<APowerDownGameState>(GameState);
	check(MyGameState);

	// Only transition if this is actually a new state
	if (NewState != MyGameState->GetCurrentState())
	{
		// Update the state, so clients know about the transition
		MyGameState->SetCurrentState(NewState);

		switch(NewState)
		{
		case EBatteryPlayState::EPlaying:
			// Activate the spawn volumes
			for (ASpawnVolume* SpawnVolume : SpawnVolumesActors)
			{
				SpawnVolume->SetSpawningActive(true);
			}
			// Start draining power
			break;

		case EBatteryPlayState::EGameOver:
			// Deactivate the spawn volumes
			for (ASpawnVolume* SpawnVolume : SpawnVolumesActors)
			{
				SpawnVolume->SetSpawningActive(false);
			}
			// Stop draining power
			GetWorldTimerManager().ClearTimer(PowerDrainTimer);
			break;

		case EBatteryPlayState::EWon:
			// Deactivate the spawn volumes
			for (ASpawnVolume* SpawnVolume : SpawnVolumesActors)
			{
				SpawnVolume->SetSpawningActive(false);
			}
			// Stop draining power
			GetWorldTimerManager().ClearTimer(PowerDrainTimer);
			break;

		case EBatteryPlayState::EUnknown:
			break;
		}
	}
}

