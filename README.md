# PowerDown

Game use Steam SDK for multiplayer.

Purpose of the game:
Each player has a certain amount of energy that decreases gradually, affecting the player’s speed of movement, the more energy the player has, the faster he moves and vice versa. The player dies if the energy supply is depleted. The energy reserve can be replenished with a set of batteries that fall in different parts of the game arena.

Goal of the player:
1. gain maximum energy before the opponent does;
2. don't stop the energy supply from running out to survive.

# PowerDown
